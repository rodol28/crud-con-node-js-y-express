const {
    products
} = require('../db.json');


module.exports = {
    getProducts: (req, res) => {
        res.json({
            products: products //tmb se podria poner solo products en lugar de products: products
        });
    },
    addProducts: (req, res) => {
        const nameProduct = req.body.name;
        products.push({
            name: nameProduct,
            id: products.length
        });
        console.log(nameProduct);
        res.json({
            'success': true,
            'msg': 'successfully added'
        });
    },
    updateProducts: (req, res) => {
        const {
            id
        } = req.params;
        const {
            name
        } = req.body;

        products.forEach((product, i) => {
            if (product.id == Number(id)) {
                product.name = name;
            }
        });

        res.json({
            'success': true,
            'msg': 'updated'
        });

    },
    deleteProducts: (req, res) => {
        const {
            id
        } = req.params;

        products.forEach((product, i) => {
            if (product.id === Number(id)) {
                products.splice(i, 1);
            }
        });

        res.json({
            'success': true,
            'msg': 'deleted'
        });
    }
};