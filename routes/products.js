const express = require('express');
const router = express.Router();
const controllers = require('../controllers/products.js')
//tambien se podria importar de la siguiente manera
// const {getProducts, addProducts} = require('../controllers/products.js');
// y en las rutas solo poner router.route('/').get(getProducts).post(addProducts) 

router.route('/')
    .get(controllers.getProducts)
    .post(controllers.addProducts);

router.route('/:id')
    .put(controllers.updateProducts)
    .delete(controllers.deleteProducts);

// router.route('*')
//     .get((req, res) => {
//         console.log("Archivo no encontrado");
//         res.end();
//     })
//     .post((req, res) => {
//         console.log("Archivo no encontrado");
//         res.end();
//     });

module.exports = router;