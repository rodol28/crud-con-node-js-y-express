const morgan = require('morgan');
const bodyParser = require('body-parser');

const express = require('express');
const app = express();

const productsRoutes = require('./routes/products.js');

// app
app.set('json spaces', 4);


// middleware
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// routes
app.use('/products', productsRoutes);




app.listen(3000, (req, res) => {
    console.log("Server listening on port 3000");
});